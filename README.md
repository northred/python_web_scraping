# Python - Web scraping

Web scraping project looking for board games at i-szop.pl

## Technologies
Project is created with:

* **Python 3.7**
* **Selenium 3.141.0**
* **Geckodriver 0.24.0** (compatibility with Mozilla Firefox 69.0.1)


## Getting Started

Copy the project to the local machine. Then change sender_email and receiver_email in index.py.


## Start

The project should be launched in a virtual environment. 
Open '{PATH}/Python_Web_scraping/venv/' in terminal and use command:

```
source bin/activate

```

Run index.py with 3 arguments "gameTitle", "maxGamePrice", "senderEmailPass". 
After a while you will receive an email with offers.
