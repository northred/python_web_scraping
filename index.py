from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import time
import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import sys

class GameSearch:
    def __init__(self, title, price, sender_pass, receiver_email="odakarol@gmail.com"): # Change receiver_email
        self.title = title
        self.price = price
        self.sender_pass = sender_pass
        self.receiver_email = receiver_email
        self.offers = []
        self.driver = object

    def seleniumOptions(self):
        options = Options()
        options.headless = True
        self.driver = webdriver.Firefox(options=options)
        self.driver.get("https://i-szop.pl/")

    def search(self):
        search = self.driver.find_element_by_xpath("//input[@id='text']")
        searchButton = self.driver.find_element_by_id("searchButton")

        search.send_keys(self.title)
        searchButton.click()
        time.sleep(5)

    def createList(self, xpath):
        elements = self.driver.find_elements_by_xpath(xpath)
        elemList = []

        if xpath.find('@href') != -1:
            for elem in elements:
                elemList.append(elem.get_attribute('href'))
        else:
            for elem in elements:
                if elem.text == "hasło:" or elem.text == "":
                    continue
                elemList.append(elem.text)
        return elemList

    def createDirectory(self):
        shopName = self.createList("//table/tbody/tr/td[2]")
        links = self.createList("//table/tbody/tr/td[2]/a[@href]")
        price = self.createList("//table/tbody/tr/td[5]")
        availability = self.createList("//table/tbody/tr/td[6]")

        for i in range(len(shopName)):
            if float(self.price) > float(price[i]):
                self.offers.append({'Sklep': shopName[i], 'Cena': price[i], 'Dostępność': availability[i], 'Link': links[i]})

    def sendEmail(self):
        if not self.offers:
            text = """\
                Brak ofert spełniających warunek cenowy. Do usłyszenia jutro.<br><br>
                   Pozdrawiam,<br>
                   Automat KI<br>
                   """
        else:
            text = """\

            Znaleziono następujące sklepy: <br><br>"""

            for offer in self.offers:
                for offer_key, offer_value in offer.items():
                    text += '<b>' + offer_key + '</b>: ' + offer_value + '<br>'
                text += '<br>'
        text += '<br>Pozdrawiam,<br>Automat KI<br>'

        port = 465
        smtp_server = "smtp.gmail.com"
        sender_email = "KI.automat@gmail.com" # Change sender_email
        password = self.sender_pass
        message = MIMEMultipart("alternative")
        message["Subject"] = "Lista sklepów z " + self.title
        message["From"] = sender_email
        message["To"] = self.receiver_email
        text = MIMEText(text, "html")
        message.attach(text)
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
            server.login(sender_email, password)
            server.sendmail(sender_email, self.receiver_email, message.as_string())

    def init(self):
        self.seleniumOptions()
        self.search()
        self.createDirectory()
        self.sendEmail()


gameTitle = sys.argv[1]
gamePrice = sys.argv[2]
emailPass = sys.argv[3]

search = GameSearch(gameTitle, gamePrice, emailPass)
search.init()

